use concrete::parser::Parse;
use wasm_bindgen::prelude::*;

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    Ok(())
}

#[wasm_bindgen]
pub fn add(a: u32, b: u32) -> u32 {
    a + b
}

#[wasm_bindgen]
pub fn uexprecho() {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let input = document
        .get_element_by_id("uexpr-in")
        .expect("could not get uexpr-in element")
        .inner_html();

    let mut parse = Parse::new(&input, concrete::root::NAMESPACE);
    let parsed_type = parse.execute().evaluate();
    let output;

    if let concrete::Type::MathMLMarkup(mmlmu) = parsed_type {
        output = mmlmu.xml_markup().as_str().to_string();
    } else {
        output = parsed_type.to_string();
    }

    document
        .get_element_by_id("uexpr-out")
        .unwrap()
        .set_inner_html(&format!("{}", output));
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
